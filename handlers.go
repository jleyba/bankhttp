package main

import (
	"fmt"
	"github.com/gojektech/heimdall/hystrix"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"html"
	"io/ioutil"
	"net/http"
)

// Return default message for root routing
func index(w http.ResponseWriter, r *http.Request) {
	log.Debug().Msg("Got root")
	fmt.Fprintf(w, "Hello %q", html.EscapeString(r.URL.Path))
}

// Handle echo
func echoHandlerH(calledServiceURL string, client *hystrix.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		params := mux.Vars(r)

		url := calledServiceURL + "/echo/" + params["message"]
		log.Debug().Msgf("URL to call: %s", url)

		headers := http.Header{}
		//headers.Set("Content-Type", "application/json")
		headers.Set("Connection", "close")

		response, errResp := client.Get(url, headers)
		if errResp != nil {
			log.Error().Msgf("Error en response: %s", errResp.Error())
			w.Header().Set("status", fmt.Sprintf("%s", http.StatusInternalServerError))
			fmt.Fprintf(w, "Error en response: %s", errResp.Error())
			return
		}

		defer response.Body.Close()

		respBody, errData := ioutil.ReadAll(response.Body)
		if errData != nil {
			log.Error().Msgf("failed to read response body %s", errData.Error())
		}

		w.Header().Set("status", fmt.Sprintf("%s", http.StatusOK))
		fmt.Fprintf(w, "%s", respBody)
	}
}

// Handle echo
func echoHandlerD(calledServiceURL string, client *http.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		params := mux.Vars(r)

		url := calledServiceURL + "/echo/" + params["message"]
		log.Debug().Msgf("URL to call: %s", url)

		headers := http.Header{}
		//headers.Set("Content-Type", "application/json")
		headers.Set("Connection", "close")

		response, errResp := client.Get(url)
		if errResp != nil {
			log.Error().Msgf("Error en response: %s", errResp.Error())
			w.Header().Set("status", fmt.Sprintf("%s", http.StatusInternalServerError))
			fmt.Fprintf(w, "Error en response: %s", errResp.Error())
			return
		}

		defer response.Body.Close()

		respBody, errData := ioutil.ReadAll(response.Body)
		if errData != nil {
			log.Error().Msgf("failed to read response body %s", errData.Error())
		}

		w.Header().Set("status", fmt.Sprintf("%s", http.StatusOK))
		fmt.Fprintf(w, "%s", respBody)
	}
}
